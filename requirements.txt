Django==2.0.1
django-cors-headers==2.4.0
cryptography==2.5
gunicorn==19.7.1
psycopg2cffi==2.8.1
pylint==1.8.2
pylint-django==0.9.0
pyOpenSSL==19.0.0
python-dateutil==2.8.0
pytz==2019.1
requests==2.20.1
rope==0.10.7
Shapely==1.6.4.post2
whitenoise==3.3.1

git+https://bitbucket.org/horizon-dev/lib-chronicle-python#egg=chronicle
