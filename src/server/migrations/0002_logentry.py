# Generated by Django 2.0.1 on 2019-05-08 14:39

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('server', '0001_create_default_superuser'),
    ]

    operations = [
        migrations.CreateModel(
            name='LogEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=200)),
                ('level', models.CharField(max_length=5)),
                ('datetime', models.DateTimeField()),
                ('data', models.TextField()),
            ],
        ),
    ]
