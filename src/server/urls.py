from django.contrib.staticfiles.storage import staticfiles_storage
from django.http import Http404
from django.urls import re_path
from django.views.defaults import page_not_found
from django.views.generic.base import RedirectView

from . import views


urlpatterns = [
    re_path(
        '^favicon.ico$',
        RedirectView.as_view(
            url=staticfiles_storage.url('images/favicon.ico'),
            permanent=False),
        name='favicon'),
    re_path(
        r'^proxy\/?$',
        views.proxy,
        name='proxy'),
    re_path(
        r'^editor.html$',
        views.map_editor,
        name='map_editor'),
    re_path(
        r'^api/v1/upload_pois$',
        views.api_v1_upload_pois,
        name='api_v1_upload_pois'),
    re_path(
        r'^api/v1/newuser$',
        views.api_v1_newuser,
        name='api_v1_newuser'),
    re_path(
        r'^api/v1/allcats$',
        views.api_v1_allcats,
        name='api_v1_allcats'),
    re_path(
        r'^api/v1/allpois$',
        views.api_v1_allpois,
        name='api_v1_allpois'),
    re_path(
        r'^api/v1/timeline$',
        views.api_v1_timeline,
        name='api_v1_timeline'),
    re_path(
        r'^api/v1/live-to-local$',
        views.api_v1_live_to_local,
        name='api_v1_live_to_local'),
    re_path(
        r'^api/v1/log_entry',
        views.api_v1_log_entry,
        name='api_v1_log_entry'),
    re_path(
        '^$',
        views.index,
        name='index'),
    re_path(
        '.*',
        page_not_found,
        {'exception' : Http404()}),
]
