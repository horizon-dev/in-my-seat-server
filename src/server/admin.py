from django import forms
from django.contrib import admin

from . import models

class LogEntryAdmin(admin.ModelAdmin):

    list_display = ('username', 'level', 'datetime', 'data',)

    list_display_links = ('username',)

admin.site.register(models.LogEntry, LogEntryAdmin)
