import os
import importlib

from django.apps import AppConfig
from django.db.models.signals import post_migrate


def set_site_name(sender, **kwargs):
    clazz = type(sender)
    fqn = clazz.__module__ + '.' + clazz.__name__
    if fqn == 'django.contrib.sites.apps.SitesConfig':
        module = importlib.import_module('django.contrib.sites.models')
        site_class = getattr(module, 'Site')
        site = site_class.objects.get(pk=1)
        sitename = os.environ['DJANGO_SITE_NAME']
        site.domain = sitename
        site.name = sitename
        site.save()


class ServerConfig(AppConfig):

    name = 'server'

    def ready(self):
        if 'DJANGO_SITE_NAME' in os.environ:
            post_migrate.connect(set_site_name)
        from . import signals
