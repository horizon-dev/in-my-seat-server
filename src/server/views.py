import json
import os
import time
import uuid
import base64

from pathlib import Path

import dateutil.parser

from django.conf import settings
from django.contrib.auth.models import User
from django.core import files
from django.http import (HttpResponse, JsonResponse)
from django.views.decorators.csrf import csrf_exempt

from django.views.generic.base import (TemplateResponseMixin, View)

import requests

import pytz

import shapely.geometry as geo

from . import models

def log(r, rb=''):
    try:
        headers = {}
        for k in r.META:
            headers[k] = str(r.META[k])
        data = {
            'timestamp': time.time(),
            'headers': headers,
            'get': r.GET,
            'post': r.POST,
            'request_body': base64.b64encode(r.body).decode('utf8'),
            'response_body': rb
        }
        print(json.dumps(data))
    except Exception as e:
        print(e)


@csrf_exempt
def proxy(request):
    url = 'http://arriva-uk.hafas.de/bin//mgate.exe'
    req = requests.post(url, request.body)
    res = req.json()
    log(request, res)
    return JsonResponse(res)

class IndexView(TemplateResponseMixin, View):

    template_name = 'index.html'

    def get(self, request):
        log(request)
        return self.render_to_response({})

index = (IndexView.as_view())

class MapEditor(TemplateResponseMixin, View):

    template_name = 'map_editor.html'

    def get(self, request, *args, **kwargs):
        return self.render_to_response({
            'GOOGLE_MAPS_API_KEY': settings.GOOGLE_MAPS_API_KEY
        })

map_editor = (MapEditor.as_view())

class ApiV1UploadPois(View):

    def get_category(self, desc):
        i = desc.rfind('<br>')
        if i > -1:
            return desc[i+4:].lower()
        return 'unknown'

    def post(self, request):
        data = json.loads(request.body.decode('UTF-8'))
        categories = []
        placemarks = {}
        if 'Placemark' in data['kml']['kml']['Document']:
            for pm in data['kml']['kml']['Document']['Placemark']:
                placemarks[pm['id']] = pm
        if 'targets' in data:
            for tid in data['targets']:
                target = data['targets'][tid]
                placemarks[target['poi']]['target'] = target
        pois = []
        poimap = {}
        for v in placemarks.values():
            coords = v['Point']['coordinates'].split(',')
            desc = v['description'] if 'description' in v else ''
            cat = self.get_category(desc)
            if cat not in categories:
                categories.append(cat)
            poi = {
                'id': v['id'],
                'name': v['name'],
                'description': desc,
                'category': self.get_category(desc),
                'latitude': float(coords[1]),
                'longitude': float(coords[0]),
            }
            pois.append(poi)
            poimap[v['id']] = poi

        f1 = os.path.join(settings.BASE_DIR, 'etc/allpois.json')
        f2 = os.path.join(settings.BASE_DIR, 'etc/allpois_with_targets.json')

        try:
            t = str(int(time.time()))
            os.rename(f1, '{}.{}'.format(f1, t))
            os.rename(f2, '{}.{}'.format(f2, t))
        except Exception:
            pass

        with Path(os.path.join(
            settings.BASE_DIR, 'etc/allpois.json')).open(mode='w') as f:
            json.dump(pois, f)
        with Path(os.path.join(
            settings.BASE_DIR, 'etc/allcats.json')).open(mode='w') as f:
            json.dump(categories, f)

        for p in poimap.values():
            p['targets'] = []

        for t in data['targets'].values():
            poi_id = t['poi']
            t.pop('poi')
            poimap[poi_id]['targets'].append(t)

        with Path(os.path.join(
            settings.BASE_DIR, 'etc/allpois_with_targets.json')).open(mode='w') as f:
            json.dump(list(poimap.values()), f)

        return JsonResponse(True, safe=False)

api_v1_upload_pois = csrf_exempt(ApiV1UploadPois.as_view())

@csrf_exempt
def api_v1_newuser(request):
    c = settings.DICTIONARY_CONNECTION.cursor()
    c.execute('SELECT word FROM words ORDER BY RANDOM() LIMIT 1')
    username = [v[0] for v in c.fetchall()][0]
    while User.objects.filter(username=username).exists():
        c.execute('SELECT word FROM words ORDER BY RANDOM() LIMIT 1')
        username = [v[0] for v in c.fetchall()][0]
    c.execute('SELECT word FROM words ORDER BY RANDOM() LIMIT 1')
    password = [v[0] for v in c.fetchall()][0]
    User.objects.create_user(username=username, password=password, email=None)
    res = {
        "id": username,
        "password": password
    }
    log(request, res)
    return JsonResponse(res)

@csrf_exempt
def api_v1_allcats(request):
    with Path(os.path.join(settings.BASE_DIR, 'etc/allcats.json')).open() as f:
        log(request)
        return HttpResponse(files.File(f), 'application/json')

@csrf_exempt
def api_v1_allpois(request):
    with Path(os.path.join(settings.BASE_DIR, 'etc/allpois.json')).open() as f:
        log(request)
        return HttpResponse(files.File(f), 'application/json')

@csrf_exempt
def api_v1_timeline(request):
    log(request)
    if request.method == 'POST':
        body = json.loads(request.body.decode('utf8'))
        with Path(os.path.join(settings.BASE_DIR, 'etc/allpois_with_targets.json')).open() as f:
            allpois = json.load(f)
        poilist = []
        for poi in allpois:
            def loop():
                for target in poi['targets']:
                    bds = list(map(lambda x: (x['lng'], x['lat']), target['bounds']))
                    poly = geo.Polygon(bds)
                    for route in body['route']:
                        pts = list(map(lambda x: (x['longitude'], x['latitude']), route))
                        line = geo.LineString(pts)
                        if line.intersects(poly):
                            return poi
                return None
            r = loop()
            if r != None:
                poilist.append(r)
        return JsonResponse(poilist, safe=False)

    with Path(os.path.join(settings.BASE_DIR, 'etc/allpois_with_targets.json')).open() as f:
        return HttpResponse(files.File(f), 'application/json')

@csrf_exempt
def api_v1_live_to_local(request):
    outdata = {
        "targets": {},
        "kml": {
            "kml": {
                "Document": {
                    "name": "Facticles",
                    "description": "",
                    "Placemark": []
                }
            }
        },
    }

    with Path(os.path.join(settings.BASE_DIR, 'etc/allpois_with_targets.json')).open() as f:
        indata = json.load(f)
        for poi in indata:
            p = {
                "id": poi["id"],
                "name": poi["name"],
                "description": poi["description"],
                "Point": {
                    "coordinates": str(poi["longitude"]) + "," + str(poi["latitude"])
                },
            }
            outdata["kml"]["kml"]["Document"]["Placemark"].append(p)
            for target in poi["targets"]:
                t = {
                    "poi": poi["id"],
                    "bounds": target["bounds"],
                    "nucleus": target["nucleus"],
                    "isGenerated": target["isGenerated"] if "isGenerated" in target else False,
                    "customText": target["customText"] if "customText" in target else "",
                }
                outdata["targets"][str(uuid.uuid4())] = t
    return JsonResponse(outdata)

@csrf_exempt
def api_v1_log_entry(request):
    log(request)
    if request.method == 'POST':
        username = request.GET.get('username')
        entries = json.loads(request.body.decode('utf8'))
        tz = pytz.timezone('UTC')
        for entry in entries:
            le = models.LogEntry()
            le.username = username
            le.datetime = tz.localize(dateutil.parser.parse(entry['datetime']))
            le.level = entry['level']
            le.data = entry['data']
            le.save()
    return JsonResponse({})

