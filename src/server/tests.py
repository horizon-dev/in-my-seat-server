from django.test import SimpleTestCase


class DummyTestCase(SimpleTestCase):

    def test_assert_always_true(self):
        self.assertIsNone(None)
