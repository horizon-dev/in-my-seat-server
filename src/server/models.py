from django.db import models

class LogEntry(models.Model):
    username = models.CharField(max_length=200)
    level = models.CharField(max_length=5)
    datetime = models.DateTimeField()
    data = models.TextField()
