export function enhance(gmaps, shape) {

  shape.prototype.getBounds = function () {
    let bounds = new gmaps.LatLngBounds();
    this.getPath().forEach(element => bounds.extend(element));
    return bounds;
  };

  // https://moduscreate.com/blog/placing-markers-inside-polygons-with-google-maps/
  shape.prototype.getApproximateCenter = function () {
    let maxSteps = 10;
    let bounds = this.getBounds();

    let center = bounds.getCenter();
    if (gmaps.geometry.poly.containsLocation(center, this)) {
      return center;
    }

    let northWest = new gmaps.LatLng(
      bounds.getNorthEast().lat(), bounds.getSouthWest().lng());

    let boundsHeight = gmaps.geometry.spherical.computeDistanceBetween(
      northWest, bounds.getSouthWest());

    let boundsWidth = gmaps.geometry.spherical.computeDistanceBetween(
      northWest, bounds.getNorthEast());

    let heightIncr = boundsHeight / maxSteps;
    let widthIncr = boundsWidth / maxSteps;

    for (let n = 1; n <= maxSteps; n++) {

      let compute = (distance, angle) =>
        gmaps.geometry.spherical.computeOffset(center, distance, angle);

      let args = [
        [(heightIncr * n), 0],
        [(widthIncr * n), 90],
        [(heightIncr * n), 180],
        [(widthIncr * n), 270]
      ];

      for (let i = 0; i < args.length; i++) {
        let pos = compute(args[i][0], args[i][1]);
        if (gmaps.geometry.poly.containsLocation(pos, this)) {
          return pos;
        }
      }
    }

    return null;
  };

  shape.prototype.toJSON = function () {
    const bounds = [];
    this.getPath().forEach((p) => {
      bounds.push(p.toJSON());
    });
    return bounds;
  };

}

export function createControl(text, titleText, callback) {

  const control = document.createElement("div");

  const ui = document.createElement("div");
  ui.className = "button";
  ui.title = titleText;
  control.appendChild(ui);

  const txt = document.createElement("div");
  txt.className = "button-text";
  txt.innerHTML = text;
  ui.appendChild(txt);

  ui.addEventListener("click", callback);

  return control;
}
