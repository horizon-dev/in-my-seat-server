import EventEmitter from "events";
import uuidv4 from "uuid/v4";
import { isUndefined } from "util";


export const EVENT_CREATED = Symbol();

export const EVENT_DESTROYED = Symbol();

export const EVENT_NUCLEUS_MOVED = Symbol();

class Target extends EventEmitter {

  constructor(gmaps, bounds, nucleus, poi, id, isGenerated, customText) {
    super();
    if (isUndefined(id)) {
      this._id = uuidv4();
    } else {
      this._id = id;
    }
    this._isGenerated = isGenerated === true;
    this.bounds = bounds;
    this.nucleus = nucleus;
    this._poi = poi;
    this._text = isUndefined(customText) ? "" : customText;
    poi.emitter.on("OPENED", () => {
      bounds.setOptions({
        strokeColor: "#00FF00",
        fillColor: "#6DFF00"
      });
    });
    poi.emitter.on("CLOSED", () => {
      if (isGenerated) {
        bounds.setOptions({
          strokeColor: "#f4bc42",
          fillColor: "#f4cb42"
        });
      } else {
        bounds.setOptions({
          strokeColor: "#006DF0",
          fillColor: "#006DF0"
        });
      }
    });
    gmaps.event.addListener(nucleus, "dragend", () => {
      this.emit(EVENT_NUCLEUS_MOVED);
    });
  }

  get id() {
    return this._id;
  }

  get poi() {
    return this._poi;
  }

  get isGenerated() {
    return this._isGenerated;
  }

  get customText() {
    return this._text;
  }

  set customText(customText) {
    this._text = customText;
  }

  destroy() {
    this.emit(EVENT_DESTROYED);
  }

  toJSON() {
    return {
      bounds: this.bounds,
      nucleus: this.nucleus.getPosition().toJSON(),
      poi: this.poi.id,
      isGenerated: this.isGenerated,
      customText: this._text
    };
  }

}

export class TargetFactory extends EventEmitter {
  createTarget(gmaps, bounds, nucleus, poi, id, isGenerated, customText) {
    const t = new Target(
      gmaps, bounds, nucleus, poi, id, isGenerated, customText);
    this.emit(EVENT_CREATED, t);
    return t;
  }
}
