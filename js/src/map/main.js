import jQuery from "jquery";
import loadGoogleMapsAPI from "load-google-maps-api";
import geolocation from "geolocation";
import Parser from "fast-xml-parser";
import uuidv4 from "uuid/v4";

import * as target from "./target.es6";
import * as MapUtils from "./map-utils.es6";
import { isUndefined, isArray } from "util";
import { EventEmitter } from "events";

class State {

  constructor (targetFactory) {
    this._kml = {};
    this._targets = {};
    targetFactory.on(target.EVENT_CREATED, t => {
      this._targets[t.id] = t;
      t.on(target.EVENT_DESTROYED, () => {
        delete this._targets[t.id];
      });
    });
  }

  addKML(kml) {
    jQuery.extend(this._kml, kml);
  }

  get kml() {
    return this._kml;
  }

  removePlacemark(id) {
    const filter = arr => {
      return jQuery.grep(arr, (pm) => {
        return pm.id != id;
      });
    };
    if (this._kml.kml.Document.Placemark) {
      this._kml.kml.Document.Placemark
          = filter(this._kml.kml.Document.Placemark);
    }
    jQuery.each(this._kml.kml.Document.Folder, (i, f) => {
      if (f.Placemark) {
        f.Placemark = filter(f.Placemark);
      }
    });
  }

  toJSON() {
    return {
      kml: this._kml,
      targets: this._targets
    };
  }

}

const GLOBAL_EMITTER = new EventEmitter();
GLOBAL_EMITTER.setMaxListeners(0);

function renderTarget(gmaps, map, targetFactory, polygon, placemark, id, nucleus, isGenerated, customText) {
  const icon = {
    url: "/static/images/target_blue_32x32.png",
    size: new gmaps.Size(32, 32),
    origin: new gmaps.Point(0, 0),
    anchor: new gmaps.Point(16, 16)
  };
  const marker = new gmaps.Marker({
    draggable: true,
    position: isUndefined(nucleus) ? polygon.getApproximateCenter() : nucleus,
    crossOnDrag: false,
    icon: icon
  });
  marker.setMap(map);
  const content = jQuery(
    "<p><strong>" + placemark.name + "</strong><br />"
    + placemark.description
    + "<br /><br />Custom text (press enter to commit): "
    + "<input type=\"text\" class=\"custom-text-box\" /></p>");
  const textBox = content.find(".custom-text-box");
  textBox.val(isUndefined(customText) ? "" : customText);
  const infowindow = new gmaps.InfoWindow({
    content: content.get(0)
  });
  let lastPos = marker.getPosition();
  gmaps.event.addListener(marker, "drag", () => {
    const p = marker.getPosition();
    if (!gmaps.geometry.poly.containsLocation(p, polygon)) {
      marker.setPosition(lastPos);
    } else {
      lastPos = p;
    }
  });
  const target =
      targetFactory.createTarget(gmaps, polygon, marker, placemark, id, isGenerated, customText);
  textBox.on("change", () => {
    target.customText = textBox.val();
  });
  const clickListener = (e) => {
    e.stop();
    target.destroy();
    polygon.setMap(null);
    marker.setMap(null);
  };
  var toggle = false;
  const leftClickListener = (e) => {
    e.stop();
    if (toggle) {
      infowindow.close();
      GLOBAL_EMITTER.emit("TARGET_INFOWINDOW_CLOSED", target.poi);
    } else {
      infowindow.open(map, marker);
      GLOBAL_EMITTER.emit("TARGET_INFOWINDOW_OPENED", target.poi);
    }
    toggle = !toggle;
  };
  gmaps.event.addListener(infowindow, "closeclick", () => {
    GLOBAL_EMITTER.emit("TARGET_INFOWINDOW_CLOSED", target.poi);
  });
  gmaps.event.addListener(infowindow, "closeclick", () => toggle = false);
  gmaps.event.addListener(polygon, "rightclick", clickListener);
  gmaps.event.addListener(marker, "rightclick", clickListener);
  gmaps.event.addListener(polygon, "click", leftClickListener);
  gmaps.event.addListener(marker, "click", leftClickListener);
}

function renderKML(kml, gmaps, map, drawingManager, targetFactory, state) {
  const renderPlacemark = (i, pm) => {
    if (isUndefined(pm.id)) {
      pm.id = uuidv4();
    }
    const coords = pm.Point.coordinates.split(",");
    const content = jQuery(
      "<p><strong>" + pm.name + "</strong><br />"
      + pm.description
      + "<br /></p>");
    const remove = jQuery("<a href=\"#\">Remove</a>");
    content.append(remove);
    const add = jQuery("<a href=\"#\">Add Target</a>");
    content.append(jQuery("<span> | </span>"));
    content.append(add);
    const infowindow = new gmaps.InfoWindow({
      content: content.get(0)
    });
    const marker = new gmaps.Marker({
      position: {
        lat: parseFloat(coords[1]),
        lng: parseFloat(coords[0])
      },
      map: map,
      title: pm.name
    });
    GLOBAL_EMITTER.on("TARGET_INFOWINDOW_OPENED", poi => {
      if (pm.id == poi.id) {
        marker.setZIndex(9999);
        marker.setIcon("https://maps.google.com/mapfiles/ms/icons/green.png");
      } else {
        marker.setZIndex(undefined);
        marker.setIcon(undefined);
      }
    });
    GLOBAL_EMITTER.on("TARGET_INFOWINDOW_CLOSED", poi => {
      if (pm.id == poi.id) {
        marker.setZIndex(undefined);
        marker.setIcon(undefined);
      }
    });
    pm.emitter = new EventEmitter();
    var toggle = false;
    marker.addListener("click", () => {
      if (toggle) {
        infowindow.close();
        pm.emitter.emit("CLOSED", pm);
      } else {
        infowindow.open(map, marker);
        pm.emitter.emit("OPENED", pm);
      }
      toggle = !toggle;
    });
    gmaps.event.addListener(infowindow, "closeclick", () => {
      pm.emitter.emit("CLOSED", pm);
    });
    remove.on("click", () => {
      infowindow.close();
      toggle = false;
      marker.setMap(null);
      state.removePlacemark(pm.id);
    });
    add.on("click", () => {
      infowindow.close();
      toggle = false;
      gmaps.event.clearListeners(drawingManager, "polygoncomplete");
      gmaps.event.addListener(drawingManager, "polygoncomplete", polygon => {
        drawingManager.setOptions({ drawingMode: null });
        polygon.setOptions({
          draggable: false,
          editable: false,
          strokeColor: "#006DF0"
        });
        renderTarget(gmaps, map, targetFactory, polygon, pm, undefined, undefined, false, undefined);
      });
      drawingManager.setOptions({
        drawingMode: gmaps.drawing.OverlayType.POLYGON
      });
    });
  };
  jQuery.each(kml.kml.Document.Placemark, renderPlacemark);
}

function initMap(gmaps, el) {
  return new Promise((resolve) => {
    const create = (lat, lng) => {
      return new gmaps.Map(el, {
        center: { lat: lat, lng: lng },
        zoom: 14
      });
    };
    geolocation.getCurrentPosition((err, pos) => {
      if (err) {
        resolve(create(52.9375237, -1.1967676));
      } else {
        resolve(create(pos.coords.latitude, pos.coords.longitude));
      }
    });
  });
}

function initImport(gmaps, map, drawingManager, targetFactory, state) {
  const inputEl = jQuery("#kml-input");
  const reader = new FileReader();
  reader.onload = (e => {
    const kml = Parser.parse(e.target.result);
    if (isUndefined(kml.kml.Document.Placemark)) {
      kml.kml.Document.Placemark = [];
    }
    if (!isArray(kml.kml.Document.Placemark)) {
      kml.kml.Document.Placemark = [kml.kml.Document.Placemark];
    }
    if (!isUndefined(kml.kml.Document.Folder)) {
      if (isArray(kml.kml.Document.Folder)) {
        jQuery.each(kml.kml.Document.Folder, (i, f) => {
          if (isArray(f.Placemark)) {
            jQuery.each(f.Placemark, (i, pm) => {
              kml.kml.Document.Placemark.push(pm);
            });
          } else if (!isUndefined(f.Placemark)) {
            kml.kml.Document.Placemark.push(f.Placemark);
          }
        });
      } else {
        if (isArray(kml.kml.Document.Folder.Placemark)) {
          jQuery.each(kml.kml.Document.Folder.Placemark, (i, pm) => {
            kml.kml.Document.Placemark.push(pm);
          });
        } else {
          kml.kml.Document.Placemark.push(kml.kml.Document.Folder.Placemark);
        }
      }
    }
    delete kml.kml.Document.Folder;
    delete kml.kml.Document.Style;
    delete kml.kml.Document.StyleMap;
    state.addKML(kml);
    renderKML(kml, gmaps, map, drawingManager, targetFactory, state);
  });
  inputEl.on("change", event => reader.readAsText(event.target.files[0]));
  const control = MapUtils.createControl(
    "Import KML",
    "Import a KML file",
    () => jQuery("#kml-input").trigger("click"));
  map.controls[gmaps.ControlPosition.TOP_CENTER].push(control);
}

function initGenerateTargets(gmaps, map, drawingManager, targetFactory, state) {
  const control = MapUtils.createControl(
    "Generate Targets",
    "Automatically generate targets",
    () => {
      const distance = 50;
      jQuery.each(state.kml.kml.Document.Placemark, (i, pm) => {

        const coords = pm.Point.coordinates.split(",");
        const c = new gmaps.LatLng(coords[1], coords[0]);
        const poly = new gmaps.Polygon({
          paths: [
            gmaps.geometry.spherical.computeOffset(c, distance, 0),
            gmaps.geometry.spherical.computeOffset(c, distance, 60),
            gmaps.geometry.spherical.computeOffset(c, distance, 120),
            gmaps.geometry.spherical.computeOffset(c, distance, 180),
            gmaps.geometry.spherical.computeOffset(c, distance, 240),
            gmaps.geometry.spherical.computeOffset(c, distance, 300)
          ],
          draggable: false,
          editable: false,
          strokeColor: "#f4bc42",
          fillColor: "#f4cb42"
        });
        poly.setMap(map);
        renderTarget(gmaps, map, targetFactory, poly, pm, undefined, undefined, true, undefined);
      });
    });

  map.controls[gmaps.ControlPosition.TOP_CENTER].push(control);
}

function initLoadMap(gmaps, map, drawingManager, targetFactory, state) {
  const inputEl = jQuery("#load-map-input");
  const reader = new FileReader();
  reader.onload = (e => {
    loadState(gmaps, map, drawingManager, targetFactory, state, e.target.result);
  });
  inputEl.on("change", event => reader.readAsText(event.target.files[0]));
  const control = MapUtils.createControl(
    "Load",
    "Load a saved map",
    () => jQuery("#load-map-input").trigger("click"));

  map.controls[gmaps.ControlPosition.TOP_CENTER].push(control);
}

function initSaveMap(gmaps, map, state) {
  const control = MapUtils.createControl(
    "Save",
    "Save the map",
    () => {
      var filename = prompt("Enter a file name", "map.json");
      if (filename) {
        var data = new Blob([JSON.stringify(state)], {type: "application/json"});
        var fr = new FileReader();
        fr.addEventListener("loadend", function () {
          var link = document.createElement("a");
          link.download = filename;
          link.href = fr.result;
          link.click();
        }, false);
        fr.readAsDataURL(data);
      }
    });

  map.controls[gmaps.ControlPosition.TOP_CENTER].push(control);
}

function initRestoreMap(gmaps, map, drawingManager, targetFactory, state) {
  const control = MapUtils.createControl(
    "Restore Live Map",
    "Restore the local map from the live map",
    () => {
      jQuery.get("/api/v1/live-to-local", (r) => {
        loadState(gmaps, map, drawingManager, targetFactory, state, r);
      }, "text");
    });

  map.controls[gmaps.ControlPosition.TOP_CENTER].push(control);
}

function initPutLive(gmaps, map, state) {
  const control = MapUtils.createControl(
    "Put Live",
    "Set the current map as the live map",
    () => {
      if (window.confirm(
        "Are you sure? This will overwrite the existing map")) {
        jQuery.post("/api/v1/upload_pois", JSON.stringify(state), () => {
          alert("The map is now live");
        });
      }
    });

  map.controls[gmaps.ControlPosition.TOP_CENTER].push(control);
}

function loadState(gmaps, map, drawingManager, targetFactory, state, data) {
  var s = JSON.parse(data);
  if (s) {
    state.addKML(s.kml);
    renderKML(s.kml, gmaps, map, drawingManager, targetFactory, state);
    const placemarks = {};
    jQuery.each(s.kml.kml.Document.Placemark, (i, pm) => {
      placemarks[pm.id] = pm;
    });
    jQuery.each(s.kml.kml.Document.Folder, (i, f) => {
      jQuery.each(f.Placemark, (i, pm) => {
        placemarks[pm.id] = pm;
      });
    });
    jQuery.each(s.targets, (id, t) => {
      const pm = placemarks[t.poi];
      var polygon = null;
      if (t.isGenerated) {
        polygon = new gmaps.Polygon({
          paths: t.bounds,
          map: map,
          draggable: false,
          editable: false,
          strokeColor: "#f4bc42",
          fillColor: "#f4cb42"
        });
      } else {
        polygon = new gmaps.Polygon({
          paths: t.bounds,
          map: map,
          draggable: false,
          editable: false,
          strokeColor: "#006DF0"
        });
      }
      renderTarget(gmaps, map, targetFactory, polygon, pm, id, t.nucleus, t.isGenerated, t.customText);
    });
  }
}

jQuery(document).ready(() => {
  loadGoogleMapsAPI({
    key: "AIzaSyAtnvNRtLtldvDVaP4_KxZQgRyDNn_Eih4",
    libraries: ["drawing", "geometry"]
  }).then(gmaps => {
    MapUtils.enhance(gmaps, gmaps.Polygon);
    initMap(gmaps, document.querySelector("#map"))
      .then(map => {
        const drawingManager = new gmaps.drawing.DrawingManager({
          drawingMode: null,
          drawingControl: false,
          map: map
        });
        const targetFactory = new target.TargetFactory();
        const state = new State(targetFactory);
        initImport(gmaps, map, drawingManager, targetFactory, state);
        initGenerateTargets(gmaps, map, drawingManager, targetFactory, state);
        initLoadMap(gmaps, map, drawingManager, targetFactory, state);
        initSaveMap(gmaps, map, state);
        initRestoreMap(gmaps, map, drawingManager, targetFactory, state);
        initPutLive(gmaps, map, state);

        const footer = jQuery("<div class=\"footer\">"
            + "<a href=\"https://chronicle.horizon.ac.uk/terms.html\">Terms &amp; Conditions</a> | "
            + "<a href=\"https://chronicle.horizon.ac.uk/terms.html\">Privacy Policy</a> | "
            + "<a href=\"https://chronicle.horizon.ac.uk/terms.html\">Cookie Policy</a>"
            + "</div>");
        map.controls[gmaps.ControlPosition.BOTTOM_CENTER].push(footer.get(0));

      });
  });
});
