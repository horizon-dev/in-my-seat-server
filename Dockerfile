FROM pypy:3.5-7.0.0-stretch
ENV PYTHONUNBUFFERED 1
EXPOSE 80
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash - \
    && apt-get install -y nodejs openssl libssl-dev libgeos-dev libgeos++-dev \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir /inmyseat
ADD js /inmyseat-js
WORKDIR /inmyseat-js
RUN npm install && npm run build
WORKDIR /inmyseat
ADD requirements.txt /inmyseat/
RUN pip install -r requirements.txt && rm requirements.txt
ADD src /inmyseat/
ADD docker-entrypoint.sh /inmyseat
RUN cp /inmyseat-js/dist/* /inmyseat/server/static/js
ENTRYPOINT [ "/inmyseat/docker-entrypoint.sh" ]
